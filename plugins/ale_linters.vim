
Plug 'dense-analysis/ale'

" Indentaciones y mas
let g:ale_linters = {
      \   'ruby': ['standardrb', 'rubocop'],
      \   'python': ['flake8', 'pylint'],
      \	  'php': ['intelephense'],
      \   'cpp': ['clang']
      \}

      "\   'javascript': ['eslint'],
