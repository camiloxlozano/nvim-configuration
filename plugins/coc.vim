
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'antoinemadec/coc-fzf'

let g:coc_global_extensions = [
    \ 'coc-emmet',
    \ 'coc-html',
    \ 'coc-git',
    \ 'coc-snippets',
    \ 'coc-json',
    \ 'coc-sql',
    \ 'coc-eslint',
    \ 'coc-prettier',
    \ 'coc-sh',
  \]

