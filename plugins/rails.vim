

Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-rails'

autocmd FileType ruby setlocal expandtab shiftwidth=2 tabstop=2
autocmd FileType eruby setlocal expandtab shiftwidth=2 tabstop=2

