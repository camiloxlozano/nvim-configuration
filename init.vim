set title
set ruler
set showcmd
set clipboard+=unnamedplus
set cursorline
set showmatch
set sw=2
set noshowmode
set relativenumber
set numberwidth=1
set mouse=a
set showmatch
set t_Co=256
set smartindent
set encoding=utf-8
set number
set backup
set backupdir=~/.config/nvim/backup/
syntax enable
filetype plugin indent on
filetype on
filetype indent on
set linespace=10
set encoding=UTF-8
set tags=tags;

"************************************************
"Key maps
"************************************************ 
let mapleader=" "
nmap <Leader>vm :edit ~/.config/nvim/init.vim<CR>
nmap <Leader>t :FloatermNew<CR> 
nmap <Leader>q :bd<CR>
nmap <Leader>h :History<CR>
nmap <Leader>bq :bufdo bd<CR>
nmap <Leader>vv ysst
nmap <Leader>s <Plug>(easymotion-s2)
nmap <Leader>N :NERDTreeFind<CR>
nnoremap <expr> <Leader>n g:NERDTree.IsOpen() ? ':NERDTreeClose<CR>' :@% == '' ? ':NERDTree<CR>' : ':NERDTreeFind<CR>'
"nmap  <Leader>f :Files<CR>

nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fs <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>



nmap <Leader>gs :Ag<CR>
nnoremap <F5> :GundoToggle<CR>
let g:ag_prg="<custom-ag-path-goes-here> --vimgrep"
nnoremap  <silent>   <tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bnext<CR>
nnoremap  <silent> <s-tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bprevious<CR>

"NeedTree


""Config Plug
call plug#begin('~/.config/nvim/plug')

source  ~/.config/nvim/plugins/material.vim
source  ~/.config/nvim/plugins/airline.vim
source  ~/.config/nvim/plugins/rails.vim
source  ~/.config/nvim/plugins/git.vim
source  ~/.config/nvim/plugins/nerdtree.vim
source  ~/.config/nvim/plugins/nerdcommenter.vim
"source  ~/.config/nvim/plugins/fzf.vim
source  ~/.config/nvim/plugins/ale_linters.vim
source  ~/.config/nvim/plugins/autosave.vim
source  ~/.config/nvim/plugins/coc.vim
source  ~/.config/nvim/plugins/floaterm.vim
Plug 'christoomey/vim-tmux-navigator'
Plug 'easymotion/vim-easymotion'
Plug 'jiangmiao/auto-pairs'
Plug 'mhinz/vim-startify'
Plug 'shougo/neocomplete.vim'
Plug 'altercation/vim-colors-solarized'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update
Plug 'rafi/awesome-vim-colorschemes'
Plug 'ryanoasis/vim-devicons'
Plug 'mhartington/oceanic-next'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-ragtag'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-lua/plenary.nvim'
call plug#end()


if has('python3')
  let g:gundo_prefer_python3 = 1
endif

"************************************************
" VIM configs
"************************************************
set ttyfast " u got a fast terminal
set lazyredraw " to avoid scrolling problems
set synmaxcol=200


"let g:airline_theme='oceanicnext'

"let g:ag_prg="<custom-ag-path-goes-here> --vimgrep"
"let g:ag_working_path_mode="r"

"syntax on
"let g:oceanic_next_terminal_bold = 1
"let g:oceanic_next_terminal_italic = 1

" For Neovim 0.1.3 and 0.1.4
let $NVIM_TUI_ENABLE_TRUE_COLOR=1

" Or if you have Neovim >= 0.1.5
if (has("termguicolors"))
  set termguicolors
endif


" Theme
syntax enable
colorscheme OceanicNext


